<div class="container my-4">

  <!-- Section -->
  <section>
    <h3 class="font-weight-bold text-center dark-grey-text pb-2">Beneficios que van a tener más de un millón de personas de la región Puno.</h3>
    <hr class="w-header my-4">
    <div class="row">
      <div class="col-md-6 col-xl-3 mb-4">
        <div class="card text-center bg-success text-white">
          <div class="card-body">
            <p class="mt-4 pt-2"><i class="fas fa-clinic-medical fa-4x"></i></p>
            <h5 class="font-weight-normal my-4 py-2"><a class="text-white text-uppercase" href="#">Más Salud</a></h5>
            <p class="mb-4">Mejorará condiciones de salud de la población, disminuendo las enfermedades de origen hídrico (diarrea, parasitosis, alergias, entre otras).</p>
          </div>
        </div>
      </div>
      <div class="col-md-6 col-xl-3 mb-4">
        <div class="card text-center bg-danger text-white">
          <div class="card-body">
            <p class="mt-4 pt-2"><i class="fas fa-cloud-sun fa-4x"></i></p>
            <h5 class="font-weight-normal my-4 py-2"><a class="text-white text-uppercase" href="#">Mejor Ambiente</a></h5>
            <p class="mb-4">Mejorará las condiciones ambientales de la región Puno. Descontaminando progresivamente nuestros ríos y el Lago Titicaca.</p>
          </div>
        </div>
      </div>
      <div class="col-md-6 col-xl-3 mb-4">
        <div class="card text-center bg-warning text-white">
          <div class="card-body">
            <p class="mt-4 pt-2"><i class="far fa-object-ungroup fa-4x"></i></p>
            <h5 class="font-weight-normal my-4 py-2"><a class="text-white text-uppercase" href="#">Mejor Turismo</a></h5>
            <p class="mb-4">Incrementará las actividades económicas de la región, especialmente las vinculadas al turismo, pesca, comercio, entre otros.</p>
          </div>
        </div>
      </div>
      <div class="col-md-6 col-xl-3 mb-4">
        <div class="card text-center bg-info text-white">
          <div class="card-body">
            <p class="mt-4 pt-2"><i class="fas fa-clinic-medical fa-4x"></i></p>
            <h5 class="font-weight-normal my-4 py-2"><a class="text-white text-uppercase" href="#">Más Salud</a></h5>
            <p class="mb-4">Mejorarás condiciones de salud de la población, disminuendo las enfermedades de origen hídrico (diarrea, parasitosis, alergias, entre otras).</p>
          </div>
        </div>
      </div>

    </div>

  </section>
  <!-- Section -->

</div>

<div class="container my-4">
	<h3 class="font-weight-bold text-center dark-grey-text pb-2">Información sobre las localidades donde se implementará los sistemas de tratamiento de aguas residuales.</h3>
	<hr class="w-header my-4">
	<div class="row row-cols-1 row-cols-md-3 g-4">
		  <div class="col-md-2">
		    <div class="card border border-success">
		      <img
		        src="https://mdbootstrap.com/img/new/standard/city/041.jpg"
		        class="card-img-top"
		        alt="..."
		      />
		      <div class="card-body">
		        <h5 class="card-title text-center">Juliaca</h5>
		      </div>
		    </div>
		  </div>
		  <div class="col-md-2">
		    <div class="card border border-danger">
		      <img
		        src="https://mdbootstrap.com/img/new/standard/city/041.jpg"
		        class="card-img-top"
		        alt="..."
		      />
		      <div class="card-body">
		        <h5 class="card-title text-center">Puno</h5>
		      </div>
		    </div>
		  </div>
		  <div class="col-md-2">
		    <div class="card border border-primary">
		      <img
		        src="https://mdbootstrap.com/img/new/standard/city/041.jpg"
		        class="card-img-top"
		        alt="..."
		      />
		      <div class="card-body">
		        <h5 class="card-title text-center">Ilave</h5>
		      </div>
		    </div>
		  </div>
		  <div class="col-md-2">
		    <div class="card border border-warning">
		      <img
		        src="https://mdbootstrap.com/img/new/standard/city/041.jpg"
		        class="card-img-top"
		        alt="..."
		      />
		      <div class="card-body">
		        <h5 class="card-title text-center">Ayaviri</h5>
		      </div>
		    </div>
		  </div>
		  <div class="col-md-2">
		    <div class="card border border-success">
		      <img
		        src="https://mdbootstrap.com/img/new/standard/city/041.jpg"
		        class="card-img-top"
		        alt="..."
		      />
		      <div class="card-body">
		        <h5 class="card-title text-center">Juli</h5>
		      </div>
		    </div>
		  </div>
		  <div class="col-md-2">
		    <div class="card border border-danger">
		      <img
		        src="https://mdbootstrap.com/img/new/standard/city/041.jpg"
		        class="card-img-top"
		        alt="..."
		      />
		      <div class="card-body">
		        <h5 class="card-title text-center">Moho</h5>
		      </div>
		    </div>
		  </div>
	</div>
</div>


<div class="container my-4">
	<div class="card-group">
	  <div class="card">
	    <img
	      src="https://mdbootstrap.com/img/new/standard/city/041.jpg"
	      class="card-img-top"
	      alt="..."
	    />
	  </div>
	  <div class="card">
	    <img
	      src="https://mdbootstrap.com/img/new/standard/city/042.jpg"
	      class="card-img-top"
	      alt="..."
	    />
	  </div>
	</div>
	<div class="card-group">
	  <div class="card">
	    <img
	      src="https://mdbootstrap.com/img/new/standard/city/041.jpg"
	      class="card-img-top"
	      alt="..."
	    />
	  </div>
	  <div class="card">
	    <img
	      src="https://mdbootstrap.com/img/new/standard/city/042.jpg"
	      class="card-img-top"
	      alt="..."
	    />
	  </div>
	</div>
</div>