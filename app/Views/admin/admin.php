<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <link rel="icon" href="img/mdb-favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
    <link rel="stylesheet" href="resources/css/bootstrap.min.css">
    <link rel="stylesheet" href="resources/css/mdb.min.css">
    <link rel="stylesheet" href="resources/css/modules/animations-extended.min.css">
    <link rel="stylesheet" href="resources/css/addons/datatables-select.min.css">
    <link rel="stylesheet" href="resources/css/addons/datatables-select2.min.css">
    <link rel="stylesheet" href="resources/css/addons/datatables.min.css">
    <link rel="stylesheet" href="resources/css/addons/datatables2.min.css">
    <link rel="stylesheet" href="resources/css/addons/directives.min.css">
    <link rel="stylesheet" href="resources/css/addons/flag.min.css">
    <link rel="stylesheet" href="resources/css/addons/jquery.zmd.hierarchical-display.min.css">
    <link rel="stylesheet" href="resources/css/addons/rating.min.css">

    <link rel="stylesheet" href="resources/css/style.css">
    <title>PTAR | ADMIN</title>
</head>
<body>

  
<div class="container-for-admin">
    <?php require_once 'navbar.php'; ?>
    <main class="pt-5 mx-lg-3">
        <div class="container-fluid mt-5">
            <div class="card mb-4 wow fadeIn">
                <div class="card-body d-sm-flex justify-content-between">
                    <h4 class="mb-2 mb-sm-0 pt-1">
                        <a href="index.php?page=dashboard">Ptar</a>
                        <span>/</span>
                        <span>Dashboard</span>
                    </h4>
                    <form class="d-flex justify-content-center">
                        <input type="search" placeholder="Buscar" aria-label="Search" class="form-control">
                        <button class="btn btn-primary btn-sm my-0 p" type="submit">
                        <i class="fa fa-search"></i>
                        </button>
                    </form>
                </div>
            </div>
            <div class="row wow fadeIn">
                <div class="col-md-9 mb-4">
                <div class="card">
                    <div class="card-body">
                    <canvas id="myChart"></canvas>
                    </div>
                </div>
                </div>
                <div class="col-md-3 mb-4">
                <div class="card mb-4">
                    <div class="card-header text-center">
                    Publicaciones
                    </div>
                    <div class="card-body">
                    <canvas id="pieChart"></canvas>
                    </div>
                </div>
                <div class="card mb-4">
                    <div class="card-body">
                    <div class="list-group list-group-flush">
                        <a class="list-group-item list-group-item-action waves-effect">Sales
                        <span class="badge badge-success badge-pill pull-right">22%
                            <i class="fa fa-arrow-up ml-1"></i>
                        </span>
                        </a>
                        <a class="list-group-item list-group-item-action waves-effect">Traffic
                        <span class="badge badge-danger badge-pill pull-right">5%
                            <i class="fa fa-arrow-down ml-1"></i>
                        </span>
                        </a>
                        <a class="list-group-item list-group-item-action waves-effect">Orders
                        <span class="badge badge-primary badge-pill pull-right">14</span>
                        </a>
                    </div>
                    </div>
                </div>
                </div>
            </div>
            <div class="row wow fadeIn">
                <div class="col-md-6 mb-4">
                <div class="card">
                    <div class="card-body">
                    <table class="table table-hover">
                        <thead class="blue-grey lighten-4">
                        <tr>
                            <th>#</th>
                            <th>Lorem</th>
                            <th>Ipsum</th>
                            <th>Dolor</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th scope="row">1</th>
                            <td>Cell 1</td>
                            <td>Cell 2</td>
                            <td>Cell 3</td>
                        </tr>
                        <tr>
                            <th scope="row">2</th>
                            <td>Cell 4</td>
                            <td>Cell 5</td>
                            <td>Cell 6</td>
                        </tr>
                        <tr>
                            <th scope="row">3</th>
                            <td>Cell 7</td>
                            <td>Cell 8</td>
                            <td>Cell 9</td>
                        </tr>
                        </tbody>
                    </table>
                    </div>
                </div>
                </div>
                <div class="col-md-6 mb-4">
                <div class="card">
                    <div class="card-body">
                    <table class="table table-hover">
                        <thead class="blue lighten-4">
                        <tr>
                            <th>#</th>
                            <th>Lorem</th>
                            <th>Ipsum</th>
                            <th>Dolor</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th scope="row">1</th>
                            <td>Cell 1</td>
                            <td>Cell 2</td>
                            <td>Cell 3</td>
                        </tr>
                        <tr>
                            <th scope="row">2</th>
                            <td>Cell 4</td>
                            <td>Cell 5</td>
                            <td>Cell 6</td>
                        </tr>
                        <tr>
                            <th scope="row">3</th>
                            <td>Cell 7</td>
                            <td>Cell 8</td>
                            <td>Cell 9</td>
                        </tr>
                        </tbody>
                    </table>
                    </div>
                </div>
                </div>
            </div>
            <div class="row wow fadeIn">
                <div class="col-lg-6 col-md-6 mb-4">
                <div class="card">
                    <div class="card-header">Line chart</div>
                    <div class="card-body">
                    <canvas id="lineChart"></canvas>
                    </div>
                </div>
                </div>
                <div class="col-lg-6 col-md-6 mb-4">
                <div class="card">
                    <div class="card-header">Radar Chart</div>
                    <div class="card-body">
                    <canvas id="radarChart"></canvas>
                    </div>
                </div>
                </div>
                <div class="col-lg-6 col-md-6 mb-4">
                <div class="card">
                    <div class="card-header">Doughnut Chart</div>
                    <div class="card-body">
                    <canvas id="doughnutChart"></canvas>
                    </div>
                </div>
                </div>
                <div class="col-lg-6 col-md-6 mb-4">
                <div class="card">
                    <div class="card-header">Horizontal Bar Chart</div>
                    <div class="card-body">
                    <canvas id="horizontalBar"></canvas>
                    </div>
                </div>
                </div>
            </div>
            <div class="row wow fadeIn">
                <div class="col-md-6 mb-4">
                <div class="card">
                    <div class="card-header">Google map</div>
                    <div class="card-body">
                    <div id="map-container" class="map-container" style="height: 500px">
                        <iframe src="https://maps.google.com/maps?q=manhatan&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0"
                        style="border:0" allowfullscreen></iframe>
                    </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </main>
    <footer class="page-footer text-center font-small primary-color-dark darken-2 mt-4 wow fadeIn">
        <div class="footer-copyright py-3">
            © 2020
        <a href="#" target="_blank"> PTAR </a>
        </div>
    </footer>
</div>
    
  <script type="text/javascript" src="resources/js/jquery.min.js"></script>
  <script type="text/javascript" src="resources/js/popper.min.js"></script>
  <script type="text/javascript" src="resources/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="resources/js/mdb.min.js"></script>
  <script type="text/javascript" src="resources/js/modules/animations-extended.min.js"></script>
  <script type="text/javascript" src="resources/js/modules/forms-free.min.js"></script>
  <script type="text/javascript" src="resources/js/modules/scrolling-navbar.min.js"></script>
  <script type="text/javascript" src="resources/js/modules/treeview.min.js"></script>
  <script type="text/javascript" src="resources/js/modules/wow.min.js"></script>
  <script type="text/javascript" src="resources/js/addons/datatables-select.min.js"></script>
  <script type="text/javascript" src="resources/js/addons/datatables-select2.min.js"></script>
  <script type="text/javascript" src="resources/js/addons/datatables.min.js"></script>
  <script type="text/javascript" src="resources/js/addons/datatables2.min.js"></script>
  <script type="text/javascript" src="resources/js/addons/directives.min.js"></script>
  <script type="text/javascript" src="resources/js/addons/flag.min.js"></script>
  <script type="text/javascript" src="resources/js/addons/imagesloaded.pkgd.min.js"></script>
  <script type="text/javascript" src="resources/js/addons/jquery.zmd.hierarchical-display.min.js"></script>
  <script type="text/javascript" src="resources/js/addons/masonry.pkgd.min.js"></script>
  <script type="text/javascript" src="resources/js/addons/rating.min.js"></script>


  <script type="text/javascript" src="resources/js/main.js"></script>
</body>
</html>