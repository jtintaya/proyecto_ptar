
<header>
    <nav class="navbar fixed-top navbar-expand-lg navbar-light white scrolling-navbar">
      <div class="container-fluid">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
              <a class="nav-link waves-effect" href="#">Notificaciones
                <span class="sr-only "></span>
              </a>
            </li>
          </ul>
          <ul class="navbar-nav nav-flex-icons">
            <li class="nav-item">
              <a href="#" class="nav-link waves-effect">
                <i class="fab fa-facebook-f"></i>
              </a>
            </li>
            <li class="nav-item">
              <a href="#" class="nav-link waves-effect">
                <i class="fab fa-twitter"></i>
              </a>
            </li>
            <li class="nav-item">
              <a href="#" class="nav-link border border-light rounded waves-effect"
               >
                <i class="fab fa-github mr-2"></i>Admin
              </a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <div class="sidebar-fixed position-fixed">
      <a class="logo-wrapper waves-effect">
        <img src="https://mdbootstrap.com/img/logo/mdb-email.png" class="img-fluid" alt="">
      </a>
      <div class="list-group list-group-flush">
        <a href="" class="list-group-item active waves-effect">
          <i class="fa fa-pie-chart mr-3"></i>Dashboard
        </a>
        <a href="#" class="list-group-item list-group-item-action waves-effect">
          <i class="fa fa-user mr-3"></i>Perfil</a>
        <a href="" class="list-group-item list-group-item-action waves-effect">
          <i class="fa fa-table mr-3"></i>Categoría</a>
        <a href="#" class="list-group-item list-group-item-action waves-effect">
          <i class="fa fa-map mr-3"></i>Beneficios</a>
        <a href="#" class="list-group-item list-group-item-action waves-effect">
          <i class="fa fa-city mr-3"></i>Provincias</a>
        <a href="#" class="list-group-item list-group-item-action waves-effect">
          <i class="fas fa-file-signature mr-3"></i>Publicaciones</a>
      </div>
    </div>
</header>