<!DOCTYPE html>
<html lang="es">
<head>

	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Ptar</title>
	<!-- Font Awesome -->
	<link
	  href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
	  rel="stylesheet"
	/>
	<!-- Google Fonts -->
	<link
	  href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
	  rel="stylesheet"
	/>
	<!-- MDB -->
	<link
	  href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/1.0.0/mdb.min.css"
	  rel="stylesheet"
	/>

</head>
<body>
	<navbar>
		<nav class="navbar navbar-expand-lg navbar-light fixed-top bg-light">
		  <!-- Container wrapper -->
		  <div class="container">
		    <!-- Navbar brand -->
		    <a class="navbar-brand" href="<?= site_url('');?>"><img src="<?= site_url('resources/images/logo.jpg');?>" alt="" width="60" height="40"></a>

		    <!-- Toggle button -->
		    <button
		      class="navbar-toggler"
		      type="button"
		      data-toggle="collapse"
		      data-target="#navbarSupportedContent"
		      aria-controls="navbarSupportedContent"
		      aria-expanded="false"
		      aria-label="Toggle navigation"
		    >
		      <span class="navbar-toggler-icon"></span>
		    </button>

		    <!-- Collapsible wrapper -->
		    <div class="collapse navbar-collapse" id="navbarSupportedContent">
		      <!-- Left links -->
		      <ul class="navbar-nav mr-auto mb-2 mb-lg-0">
		        <li class="nav-item">
		          <a class="nav-link active" aria-current="page" href="<?= site_url('');?>">Inicio</a>
		        </li>
		        <li class="nav-item">
		          <a class="nav-link" href="<?= site_url('nosotros');?>">Nosotros</a>
		        </li>
		        <li class="nav-item">
		          <a class="nav-link" href="<?= site_url('accion');?>">Lineas de acción</a>
		        </li>
		        <li class="nav-item">
		          <a class="nav-link" href="<?= site_url('prensa');?>">Notas de prensa</a>
		        </li>
		        <li class="nav-item">
		          <a class="nav-link" href="partes">Mesa de partes</a>
		        </li>

		        <!-- Navbar dropdown -->

		        <!-- <li class="nav-item dropdown">
		          <a
		            class="nav-link dropdown-toggle"
		            href="#"
		            id="navbarDropdown"
		            role="button"
		            data-toggle="dropdown"
		            aria-expanded="false"
		          >
		            Dropdown
		          </a>
		          <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
		            <li><a class="dropdown-item" href="#">Action</a></li>
		          </ul>
		        </li> -->

		      </ul>
		      <!-- Left links -->

		      <!-- Search form -->
		      <form class="d-flex input-group w-auto">
		        <input
		          type="search"
		          class="form-control"
		          placeholder="Buscar"
		          aria-label="Search"
		        />
		        <button class="btn btn-outline-primary" type="button" data-ripple-color="dark">
		          <i class="fas fa-search"></i>
		        </button>
		      </form>
		    </div>
		    <!-- Collapsible wrapper -->
		  </div>
		  <!-- Container wrapper -->
		</nav>
	</navbar>