<div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img
        src="<?=site_url('resources/images/portada.jpg');?>"
        class="d-block w-100"
        alt="..."
      />
      <div class="carousel-caption d-none d-md-block">
        <button class="btn btn-warning">Conoce Más</button>
      </div>
    </div>
  </div>
</div>