
	<footer class="bg-light text-center text-lg-left">
	  <!-- Grid container -->
	  <div class="container p-4 pb-0">
	    <form action="">
	      <!--Grid row-->
	      <div class="row justify-content-center">
	        <!--Grid column-->
	        <div class="col-auto mb-4 mb-md-0">
	          <p class="pt-2"><strong>Únete a nuestra lista de correo y recibe directamente artículos, noticias y mucho más sobre los avances del proyecto.</strong></p>
	        </div>
	        <!--Grid column-->

	        <!--Grid column-->
	        <div class="col-md-5 col-12 mb-4 mb-md-0">
	          <!-- Email input -->
	          <div class="form-outline mb-4">
	            <input type="email" id="form5Example2" class="form-control" />
	            <label class="form-label" for="form5Example2">Ingresa tu correo electrónico</label>
	          </div>
	        </div>
	        <!--Grid column-->

	        <!--Grid column-->
	        <div class="col-auto mb-4 mb-md-0">
	          <!-- Submit button -->
	          <button type="submit" class="btn btn-primary mb-4">
	            Subscribe
	          </button>
	        </div>
	        <!--Grid column-->
	      </div>
	      <!--Grid row-->
	    </form>
	  </div>
	  <!-- Grid container -->

	  <!-- Copyright -->
	  <div class="text-center p-3 bg-dark">
	    © 2020
	    <a class="text-white" href="<?= site_url('');?>">PTAR</a>
	  </div>
	  <!-- Copyright -->
	</footer>

	<!-- MDB -->
	<script
	  type="text/javascript"
	  src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/1.0.0/mdb.min.js"
	></script>
</body>
</html>